import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {ProjectMaterialModule} from './modules/project-material/project-material.module';
import { InfoPageComponent } from './info-page/info-page.component';
import { UsersPageComponent } from './users-page/users-page.component';
import { JobCategoriesPageComponent } from './job-categories-page/job-categories-page.component';
import {RouterModule} from '@angular/router';
import { CitesPageComponent } from './cites-page/cites-page.component';
import { EducationPageComponent } from './education-page/education-page.component';
import { PackagesPageComponent } from './packages-page/packages-page.component';
import { JobsPageComponent } from './jobs-page/jobs-page.component';
import { StatisticsPageComponent } from './statistics-page/statistics-page.component';

const routes = [
    {path: '',
    redirectTo: '/info',
    pathMatch: 'full'},
    {path: 'info', component: InfoPageComponent},
    {path: 'users', component: UsersPageComponent},
    {path: 'job-categories', component: JobCategoriesPageComponent},
    {path: 'cites', component: CitesPageComponent},
    {path: 'education', component: EducationPageComponent},
    {path: 'packages', component: PackagesPageComponent},
    {path: 'jobs', component: JobsPageComponent},
    {path: 'statistics', component: StatisticsPageComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    InfoPageComponent,
    UsersPageComponent,
    JobCategoriesPageComponent,
    CitesPageComponent,
    EducationPageComponent,
    PackagesPageComponent,
    JobsPageComponent,
    StatisticsPageComponent
  ],
  imports: [
    BrowserModule,
    ProjectMaterialModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
