import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitesPageComponent } from './cites-page.component';

describe('CitesPageComponent', () => {
  let component: CitesPageComponent;
  let fixture: ComponentFixture<CitesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
