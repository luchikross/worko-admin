import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonToggleModule} from '@angular/material/button-toggle';

@NgModule({
  imports: [
    CommonModule,
    MatMenuModule,
    MatButtonToggleModule
  ],
  exports: [
    MatMenuModule,
    MatButtonToggleModule
  ],
  declarations: []
})
export class ProjectMaterialModule { }
