import { ProjectMaterialModule } from './project-material.module';

describe('ProjectMaterialModule', () => {
  let projectMaterialModule: ProjectMaterialModule;

  beforeEach(() => {
    projectMaterialModule = new ProjectMaterialModule();
  });

  it('should create an instance', () => {
    expect(projectMaterialModule).toBeTruthy();
  });
});
