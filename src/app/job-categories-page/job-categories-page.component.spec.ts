import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobCategoriesPageComponent } from './job-categories-page.component';

describe('JobCategoriesPageComponent', () => {
  let component: JobCategoriesPageComponent;
  let fixture: ComponentFixture<JobCategoriesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobCategoriesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobCategoriesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
