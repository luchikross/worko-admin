import { Component } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  menu = [
      {item: 'Info', page: 'info' },
      {item: 'Users', page: 'users' },
      {item: 'Job categories', page: 'job-categories' },
      {item: 'Cites', page: 'cites' },
      {item: 'Education', page: 'education' },
      {item: 'Packages', page: 'packages' },
      {item: 'Jobs', page: 'jobs' },
      {item: 'Statistics', page: 'statistics' }
  ];

  constructor(private router: Router) { }

  navClick(item){
      console.log(item);
      this.router.navigate(['/' + item]);
  }
}
